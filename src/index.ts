import express from 'express';
import { getStatus, Status } from './status';

const app = express();

let currentStatus: Status = {} as any;

const fetchLoop = async () => {
    currentStatus = await getStatus();
    setTimeout(fetchLoop, 1000 * 60);
};

app.get('/status', async (_, res) => {
    res.json(currentStatus);
});

app.listen(8000);
fetchLoop();
