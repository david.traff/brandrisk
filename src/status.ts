import Tesseract from 'tesseract.js';

type Level = '1' | '2' | '3' | '4' | '5' | '5E' | 'Okänd';

export interface Status {
    level: Level;
    date: string;
    description: string;
}

const url = 'https://atom.cssuite.se/webapp/riskbild/?key=3Ca12S34Sa11vvxadfeqs&type=png&nocolor=1&bgcolor=ffffff&nopadding=1';
const levels: Level[] = ['1', '2', '3', '4', '5', '5E', 'Okänd'];

const getLevel = (input: string): Level | undefined => {
    return levels.find((level) => input.startsWith(level));
};

export const getStatus = async (): Promise<Status> => {
    const { data } = await Tesseract.recognize(url, 'swe');

    const lines = data.lines
        .map((line) => line.text)
        .map((line) => line.trim());

    const date = lines[lines.length - 1].trim();
    const levels = lines.map(getLevel);
    const levelIndex = levels.findIndex((level) => !!level);

    const level = levels[levelIndex] || 'Okänd';

    const descriptions = lines.slice(0, lines.length - 1);

    if (levelIndex >= 0) {
        descriptions[levelIndex] = descriptions[levelIndex].substr(level.length);
    }

    return {
        level,
        date,
        description: descriptions.join('\r\n').trim()
    };
};