FROM node:14.4.0-alpine3.11 AS builder

WORKDIR /usr/src/brandrisk

COPY package*.json ./
COPY tsconfig*.json ./
COPY src src

RUN npm install
RUN npm run build

FROM node:14.4.0-alpine3.11

ENV NODE_ENV=production
WORKDIR /usr/src/brandrisk

COPY package*.json ./
RUN npm install
COPY --from=builder /usr/src/brandrisk/dist dist/

EXPOSE 8000

CMD ["npm", "start"]